"""
    Space Game by Gordon Poole

    An Asteroids clone I made while learning Pyglet.
"""

import pyglet
import math
import random
import utilities
import sys

# FIXME: Asteroids sometimes spawn inside each other.
# FIXME: VERY slow memory leak.
# NOTE: Possibly fixed?

class GameWindow(pyglet.window.Window):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def on_draw(self):
        self.clear()
        gameBatch.draw()
        scoreLabel.draw()

    def update(self, dt):
        global keydata, gameObjects, bulletObjects, score, removeMe, totalAsteroids

        # Signify that a tick has gone by
        clock.tick()

        # Set our window's title to our FPS
        window.set_caption("FPS: " + str(clock.get_fps()))

        for go in gameObjects:
            go.update(dt)

            if(go.y > window.height):
                go.y = 0
            elif(0 > go.y):
                go.y = window.height

            if(go.x > window.width):
                    go.x = 0
            elif(0 > go.x):
                go.x = window.width

            # If the asteroid gets too close to the ship, destroy the ship
            if(isinstance(go, Asteroid)):
                if(utilities.getDistance(go, gameObjects[0]) < go.width/2):
                    # Add ship to removeMe list
                    removeMe.append(gameObjects[0])

        for b in bulletObjects:
            b.update(dt)

            for go in gameObjects:
                if(isinstance(go, Asteroid)):
                    if(utilities.getDistance(b, go) < go.width/2):
                        removeMe.append(go)
                        removeMe.append(b)
                        change_score(5)

            if(b.y > window.height):
                removeMe.append(b)
            elif(0 > b.y):
                removeMe.append(b)

            if(b.x > window.width):
                removeMe.append(b)
            elif(0 > b.x):
                removeMe.append(b)

        if removeMe:
            for o in removeMe:
                if(isinstance(o, Asteroid)):
                    if(o.scale >= 1):
                        gameObjects.append(Asteroid(o.x - 32, o.y, o.scale/2))
                        gameObjects.append(Asteroid(o.x + 32, o.y, o.scale/2))
                    gameObjects.remove(o)
                elif(isinstance(o, Bullet)):
                    # Double check if the bullet hasn't already been removed
                    if(o in bulletObjects):
                        bulletObjects.remove(o)
                elif(isinstance(o, Ship)):
                    print("You lose!")
                    sys.exit()

        if(score == totalAsteroids * 35):
            print("You win!  Your final score was " + str(score))
            sys.exit()

        removeMe.clear()


class Ship(pyglet.sprite.Sprite):
    def __init__(self, x, y):
        self.velocity = [random.randint(-50, 50), random.randint(-50, 50)]
        self._speedCap = 1000
        self._shootCooldown = 1
        self._currentCooldown = 1

        pyglet.sprite.Sprite.__init__(self, images["ship"], x = x, y = y, batch = gameBatch)

    def update(self, dt):
        global DECEL_CONSTANT

        self._currentCooldown += dt

        # Movement controls
        if(keydata[pyglet.window.key.UP]):
            self.velocity[0] += 5 * math.sin(math.radians(self.rotation));
            self.velocity[1] += 5 * math.cos(math.radians(self.rotation));

        if(keydata[pyglet.window.key.LEFT]):
            self.rotation -= 150 * dt
        elif(keydata[pyglet.window.key.RIGHT]):
            self.rotation += 150 * dt

        if(keydata[pyglet.window.key.SPACE] and self._currentCooldown > self._shootCooldown):
            bulletObjects.append(Bullet(gameObjects[0].x, gameObjects[0].y, gameObjects[0].rotation))
            self._currentCooldown = 0

        # Movement
        self.x += self.velocity[0] * dt
        self.y += self.velocity[1] * dt

        # Caps the horizontal movespeed
        if(self.velocity[0] > self._speedCap):
            self.velocity[0] = self._speedCap
        elif(self.velocity[0] < -self._speedCap):
            self.velocity[0] = -self._speedCap

        # Caps the vertical movespeed
        if(self.velocity[1] > self._speedCap):
            self.velocity[1] = self._speedCap
        elif(self.velocity[1] < -self._speedCap):
            self.velocity[1] = -self._speedCap

        # Deceleration
        if(self.velocity[0] - DECEL_CONSTANT > 0):
            self.velocity[0] -= DECEL_CONSTANT
        elif(self.velocity[0] > 0):
            self.velocity[0] = 0
        elif(self.velocity[0] + DECEL_CONSTANT < 0):
            self.velocity[0] += DECEL_CONSTANT
        elif(self.velocity[0] < 0):
            self.velocity[0] = 0

        if(self.velocity[1] - DECEL_CONSTANT > 0):
            self.velocity[1] -= DECEL_CONSTANT
        elif(self.velocity[1] > 0):
            self.velocity[1] = 0
        elif(self.velocity[1] + DECEL_CONSTANT < 0):
            self.velocity[1] += DECEL_CONSTANT
        elif(self.velocity[1] < 0):
            self.velocity[1] = 0

class Asteroid(pyglet.sprite.Sprite):
    def __init__(self, x = None, y = None, newscale = 2):
        global images

        self.velocity = [random.randint(-100, 100), random.randint(-100, 100)]

        if(x == None):
            if(random.random() > 0.5):
                x = random.randint(100 + window.get_size()[0]/2, window.get_size()[0])
            else:
                x = random.randint(0, window.get_size()[0]/2) - 100

        if(y == None):
            y = random.randint(0, window.get_size()[1])

        super().__init__(images["meteor"], x = x, y = y, batch = gameBatch)
        self.scale = newscale

    def update(self, dt):
        global gameObjects

        for go in gameObjects:
            if(isinstance(go, Asteroid) and go is not self):
                if(utilities.getDistance(self, go) < go.width):
                    # Reverse our direction
                    self.velocity[0], self.velocity[1] = -self.velocity[0], -self.velocity[1]

        self.x += self.velocity[0] * dt
        self.y += self.velocity[1] * dt

class Bullet(pyglet.sprite.Sprite):
    def __init__(self, x, y, direction):
        # Set up our velocity
        self.velocity = [0, 0]

        # Add initial velocity
        self.velocity[0] = 500 * math.sin(math.radians(direction))
        self.velocity[1] = 500 * math.cos(math.radians(direction))

        pyglet.sprite.Sprite.__init__(self, images["bullet"], x = x, y = y, batch = gameBatch)

    def update(self, dt):
        self.x += self.velocity[0] * dt
        self.y += self.velocity[1] * dt

def change_score(s):
    global score, scoreLabel

    score += s
    scoreLabel.text = "Score: " + str(score)

def trace_leaks(dt):
    global gameObjects, bulletObjects, removeMe

    print("gameObjects: %s" % (len(gameObjects)))
    print("bulletObjects: %s" % (len(bulletObjects)))
    print("removeMe: %s" % (len(removeMe)))

if __name__ == "__main__":
    totalAsteroids = input("Please enter the amount of asteroids you would like: ")
    totalAsteroids = int(totalAsteroids)

    if(totalAsteroids > 20):
        answer = input("%s, are you SURE?  That's a LOT of asteroids!  (y/n)" % totalAsteroids)
        if(answer == "n"):
            print("Exitting...")
            sys.exit()
        elif(answer == "y"):
            print("Very well...")
        else:
            print("\"%s\" is not a valid choice.  Exitting..." % answer)
            sys.exit()

    gameObjects = [] # Stores all of our current game objects
    bulletObjects = []
    removeMe = []

    window = GameWindow(width = 1024, height = 600, vsync = False)
    gameBatch = pyglet.graphics.Batch()
    images = {"meteor" : pyglet.image.load("assets/meteor.png"), "bullet" : pyglet.image.load("assets/bullet.png"), "ship" : pyglet.image.load("assets/ship.png")}

    # Center the x and y of our images
    for k, v in images.items():
        v.anchor_x = int(v.width/2)
        v.anchor_y = int(v.height/2)

    gameObjects.append(Ship(window.get_size()[0]/2, window.get_size()[1]/2)) # gameObjects[0] == ship

    for i in range(totalAsteroids):
        gameObjects.append(Asteroid())

    score = 0
    scoreLabel = pyglet.text.Label("Score: " + str(score), x = window.width/2, y = window.height, anchor_x = "center", anchor_y = "top")

    clock = pyglet.clock.Clock()

    DECEL_CONSTANT = 0.1

    keydata = pyglet.window.key.KeyStateHandler()
    window.push_handlers(keydata)
    pyglet.clock.schedule(window.update)
    pyglet.app.run()
