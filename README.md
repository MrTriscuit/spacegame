# README #

This is a clone of the classic Asteroid game written in Python, using the Pyglet library.  I've written this while learning Pyglet to get a feel for it.

### Requirements? ###

* Any potato of a computer can run this.
* You WILL need Python installed to run this from source.  I'm using 3.5, I recommend you do the same.

### Known bugs ###

* Asteroids sometimes spawn within each other.