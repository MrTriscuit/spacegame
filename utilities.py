"""
    Utilities class by Gordon Poole

    Contains various generic methods for game creation.
"""

import pyglet, math

def drawRect(x, y, width, height, color = (255, 255, 255)): # Draws a rectangle at [x, y] with width, height size # FIXME: Distorts very badly
    pyglet.graphics.draw(4, pyglet.gl.GL_QUADS,
    ('v2f', [x - width, y, x, y, x, y - height, x - width, y - height]),
    ('c3B', color * 4))
    # print (x, y, width, height)

def checkCollision(spritea, spriteb): # TODO: Finish me
    drawRect(spritea.x, spritea.y, spritea.width, spritea.height, (0, 255, 255))
    drawRect(spriteb.x, spriteb.y, spriteb.width, spriteb.height, (0, 0, 255))
    if(spritea.x > spriteb.x and spritea.x < spriteb.x + spriteb.width): # X check done, TODO: y check
        print("Collision!")
        return True

def getDistance(spritea, spriteb):
    # print(math.sqrt((spriteb.x - spritea.x)**2 + (spriteb.y - spritea.y)**2))
    return math.sqrt((spriteb.x - spritea.x)**2 + (spriteb.y - spritea.y)**2)
